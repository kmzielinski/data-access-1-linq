﻿namespace DataAccess.Models
{
    public class WifiAccessPoint
    {
        public string Id { get; set; }
        public string Localization { get; set; }
        public float CoordinateX { get; set; }
        public float CoordinateY { get; set; }
    }
}