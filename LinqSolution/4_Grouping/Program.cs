﻿using System;
using DataAccess;

namespace _4_Grouping
{
    class Program
    {
        static void Main(string[] args)
        {
            var dataService = new DataService();

            // prevent closing the console
            Console.ReadLine();
        }
    }
}